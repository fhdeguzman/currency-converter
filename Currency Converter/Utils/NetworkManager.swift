//
//  NetworkManager.swift
//  Currency Converter
//
//  Created by Franz Henri de Guzman on 9/24/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public typealias JSONDictionary = [String: Any]
public typealias SuccessBlock = (JSONDictionary) -> ()
public typealias FailureBlock = () -> ()
public typealias ResponseFailureBlock = (String) -> ()

final class NetworkManager: NSObject {

    class var shared: NetworkManager {
        struct Static {
            static let instance: NetworkManager = NetworkManager()
        }
        return Static.instance
    }

    private func converterAPI(url: String, success:  @escaping SuccessBlock, failure:  @escaping FailureBlock) {
          print(url)
        guard let url = URL(string: url) else {
            failure()
            return
        }

        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 45.0)
        urlRequest.httpMethod = "GET"


        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in

          
            guard let data = data else {
                failure()
                return
            }

            do {
                let responseJSON = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? JSONDictionary
                guard let response = responseJSON else {
                    failure()
                    return
                }
                print(response)
                success(response)
            } catch {
                print(error.localizedDescription)
                failure()
            }
            }.resume()
    }

    public func convert(_ success:  @escaping SuccessBlock, failure:  @escaping FailureBlock) {
        guard let amount = UserDefaults.standard.string(forKey: "amount") else { return }
        guard let currency = UserDefaults.standard.string(forKey: "currency") else { return }
        guard let convertCurrency = UserDefaults.standard.string(forKey: "convertCurrency") else { return }
        let convert = "\(amount)-\(currency)/\(convertCurrency)/"
        let url = Constants.baseURL + convert + "latest"
        self.converterAPI(url: url, success: { (response) in
            // Success
            success(response)
        }) {
            // Failed
            failure()
            print("failed response")
        }
    }
    
    public func getAllCurrency(_ success:  @escaping SuccessBlock, failure:  @escaping FailureBlock) {
        self.converterAPI(url: Constants.allCurrencyURL, success: { (response) in
            // Success
            success(response)
        })
        {
            // Failed
            failure()
            print("failed response")
        }
        
    }
}

