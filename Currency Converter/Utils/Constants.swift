//
//  Constants.swift
//  Currency Converter
//
//  Created by Franz Henri de Guzman on 9/25/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import Foundation


struct Constants {
    static let baseURL = "http://api.evp.lt/currency/commercial/exchange/"
    static let allCurrencyURL = "https://api.exchangeratesapi.io/latest"
}
