//
//  Extensions.swift
//  Currency Converter
//
//  Created by Franz Henri de Guzman on 9/24/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}


extension UITextField {
    func addBottomBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = UIColor.lightGray.cgColor
        borderStyle = .none
        layer.addSublayer(bottomLine)
    }
}

class ConverterHelper {
    
    class func getAllCurrency(_ success:  @escaping (CurrencyRates) -> (), failure:  @escaping ResponseFailureBlock) {
        NetworkManager.shared.getAllCurrency({ (responseData) in
            var allCurrency = [String]()
            
            let rates = responseData as! NSDictionary
            var ratesList = CurrencyRates(result: rates)
            
            success(ratesList)
        }) {
            failure("Something went wrong")
        }
    }
    
}
