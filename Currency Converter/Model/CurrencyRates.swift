//
//  Currency.swift
//  Currency Converter
//
//  Created by Franz Henri de Guzman on 9/25/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import Foundation
import SwiftyJSON

struct CurrencyRates {
    var base: String
    var rates: [Currency] = []
    
    enum  CodingKeys: String, CodingKey {
        case baseCurrency
        case rates
    }
    
    init(result: NSDictionary){
        let getRates = JSON(result)
        self.base = getRates["base"].stringValue
        let rateList = getRates["rates"].dictionaryValue
        for rate in rateList{
            rates.append(Currency(value: rate.value.doubleValue, currency: rate.key))
        }
    }

    
}
