//
//  Currency.swift
//  Currency Converter
//
//  Created by Franz Henri de Guzman on 9/25/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import Foundation

struct CurrencyWallet: Codable {
    var amount: Double
    var currency: String

    enum  CodingKeys: String, CodingKey {
        case amount
        case currency
    }
}
