//
//  Currency.swift
//  Currency Converter
//
//  Created by Franz Henri de Guzman on 9/25/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Currency: Codable {
    let value: Double
    let currency: String
    

    enum  CodingKeys: String, CodingKey {
        case value
        case currency
    }
}

