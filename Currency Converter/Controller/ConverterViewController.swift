//
//  ConverterViewController.swift
//  Currency Converter
//
//  Created by Franz Henri de Guzman on 9/24/20.
//  Copyright © 2020 Franz Henri de Guzman. All rights reserved.
//

import UIKit
import MTSlideToOpen
import iOSDropDown


class ConverterViewController: UIViewController, MTSlideToOpenDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate {
    
    
    
    @IBOutlet weak var sellTextfield: UITextField!
    @IBOutlet weak var receivedTextfield: UITextField!
    @IBOutlet weak var submitBtn: MTSlideToOpenView!
    @IBOutlet weak var sellDropDown: DropDown!
    @IBOutlet weak var receiveDropDown: DropDown!
    @IBOutlet weak var currencyCollectionView: UICollectionView!
    
    
    
    var conversionTries: Int! = 0
    var commissionEnabled: Bool! = false
    var commission: Double = 0.0
    var sell: Currency?
    var receive: Currency?
    var walletList = [CurrencyWallet]()
    static var currencyRates: CurrencyRates?
    var converterCounter = 0
    var currentWalletBalance: CurrencyWallet?
    static var convertedAmount: CurrencyWallet?
    var walletCurrencies = [String]()
    var convertedAmountInBase: Double = 0.0
    var timer = Timer()
    var timerCounter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSliderBtn()
        
        navigationItem.title = "Currency Converter"
        navigationController?.navigationBar.barTintColor = UIColor(red: 26/255, green: 152/255, blue: 252/255, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        
        self.getAllCurrency()
        
        currencyCollectionView.delegate = self
        
        sellDropDown.addBottomBorder()
        receiveDropDown.addBottomBorder()
        sellTextfield.addBottomBorder()
        receivedTextfield.addBottomBorder()
        
        currentWalletBalance = CurrencyWallet(amount: 1000, currency: "EUR")
        walletList.append(currentWalletBalance!)
        currencyCollectionView.dataSource = self
        sellTextfield.delegate = self
        
        // updateWallet(usd: usdString, eur: eurString, jpy: jpyString)
        
         timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
    }
    
    
    private func setUpSliderBtn() {
        submitBtn.sliderViewTopDistance = 0
        submitBtn.sliderCornerRadius = 28
        submitBtn.showSliderText = true
        submitBtn.labelText = "SUBMIT"
        submitBtn.thumbnailColor = UIColor(red:141.0/255, green:19.0/255, blue:65.0/255, alpha:1.0)
        submitBtn.slidingColor = UIColor.red
        submitBtn.textColor = UIColor.orange
        submitBtn.delegate = self
        submitBtn.thumnailImageView.image = #imageLiteral(resourceName: "ic_arrow").imageFlippedForRightToLeftLayoutDirection()
        
    }
    
    @objc func timerAction() {
        timerCounter += 1
        if timerCounter > 5{
            getAllCurrency()
            timerCounter = 0
        }
        
    }
    
    
    var allCurrency = [String]()
    
    func getAllCurrency(){
        ConverterHelper.getAllCurrency({ (result) in
            print(result)
            self.allCurrency.removeAll()
            ConverterViewController.currencyRates?.rates.removeAll()
            self.walletCurrencies.removeAll()
            ConverterViewController.currencyRates = result
            for currency in result.rates{
                self.allCurrency.append(currency.currency)
            }
            ConverterViewController.currencyRates?.rates.append(Currency(value: 1, currency: "EUR"))
            self.allCurrency.append(ConverterViewController.currencyRates!.base)
            self.setupDropDown(dropDown: self.sellDropDown, textField: self.sellTextfield)
            self.setupDropDown(dropDown: self.receiveDropDown, textField: self.receivedTextfield)
            
            
        }) { (error) in
            self.showAlert(msg: error, title: "Error")
        }
    }
    
    
    private func setupDropDown(dropDown: DropDown, textField: UITextField) {
        
        if dropDown == self.sellDropDown{
            for currency in walletList{
                       walletCurrencies.append(currency.currency)
                   }
            dropDown.optionArray = walletCurrencies
        }
            
        else{
            dropDown.optionArray = allCurrency.sorted{ $0 < $1 }
        }
        
        dropDown.didSelect { (selectedCurrency, index, id) in
            dropDown.text = selectedCurrency
            
            if dropDown == self.sellDropDown{
                
                if let i = ConverterViewController.currencyRates?.rates.firstIndex(where: {$0.currency == selectedCurrency}) {
                    self.sell = ConverterViewController.currencyRates?.rates[i]
                    if self.receive != nil && self.sellTextfield.text!.count > 0{
                        
                        if selectedCurrency != ConverterViewController.currencyRates?.base{
                            self.convertedAmountInBase = (self.sellTextfield.text!.toDouble()!/self.sell!.value)
                            self.receivedTextfield.text = "\((self.convertedAmountInBase * (self.receive?.value)!).rounded(toPlaces: 2))"
                            ConverterViewController.convertedAmount = CurrencyWallet(amount: (self.receivedTextfield.text?.toDouble())!, currency: self.receive!.currency)
                        }
                        else{
                            self.receivedTextfield.text = "\((self.sellTextfield.text!.toDouble()! * (self.receive?.value)!).rounded(toPlaces: 2))"
                            ConverterViewController.convertedAmount = CurrencyWallet(amount: (self.receivedTextfield.text?.toDouble())!, currency: self.receive!.currency)
                        }
                    }
                }
            }
                
            else{
                
                if let i = ConverterViewController.currencyRates?.rates.firstIndex(where: {$0.currency == selectedCurrency}) {
                    self.receive = ConverterViewController.currencyRates?.rates[i]
                    if self.sell != nil && self.sellTextfield.text!.count > 0{
                        
                        if self.sell?.currency != ConverterViewController.currencyRates?.base{
                            self.convertedAmountInBase = (self.sellTextfield.text!.toDouble()!/self.sell!.value)
                            self.receivedTextfield.text = "\(( self.convertedAmountInBase * (self.receive?.value)!).rounded(toPlaces: 2))"
                            ConverterViewController.convertedAmount = CurrencyWallet(amount: (self.receivedTextfield.text?.toDouble())!, currency: self.receive!.currency)
                        }
                            
                        else{
                            self.receivedTextfield.text = "\(( self.sellTextfield.text!.toDouble()! * (self.receive?.value)!).rounded(toPlaces: 2))"
                            ConverterViewController.convertedAmount = CurrencyWallet(amount: (self.receivedTextfield.text?.toDouble())!, currency: self.receive!.currency)
                            self.convertedAmountInBase = ConverterViewController.self.convertedAmount!.amount
                        }
                    }
                }
            }
        }
    }
    
    
    
    private func validateConversionTries(_: Int) {
        self.commissionEnabled = self.conversionTries >= 6 ? true : false
        switch self.commissionEnabled {
        case true:
            if let multiplier = self.sellTextfield.text?.toDouble() {
                self.commission = multiplier * 0.007
            }
        case false:
            return
        default:
            break
        }
    }
    
    
    
    private func showAlert( msg: String, title: String) {
        let alert = UIAlertController(title: title, message: "\(msg)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
        
    }
    
    private func setUserDefaults() {
        UserDefaults.standard.set(sellTextfield.text, forKey: "amount")
        UserDefaults.standard.set(sellDropDown.text, forKey: "currency")
        UserDefaults.standard.set(receiveDropDown.text, forKey: "convertCurrency")
    }
    
    private func resetUserDefaults() {
        if let appDomain = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: appDomain)
        }
    }
    
    
    //SLIDER BTN DELEGATE
    func mtSlideToOpenDelegateDidFinish(_ sender: MTSlideToOpenView) {
        
        converterCounter += 1
        
        if sellTextfield.text!.trimmingCharacters(in: .whitespaces).isEmpty {
            self.showAlert(msg: "Sell Amount is Empty", title: "Error")
        }else{
            var charge = 0.00
            if converterCounter > 5{
                charge = (sellTextfield.text?.toDouble())! * (0.007)
            }
            
            if let i = walletList.firstIndex(where: { $0.currency == sell?.currency }) {
                
                if (sellTextfield.text?.toDouble())! > walletList[i].amount{
                    showAlert(msg: "Error converting. Input amount is greater than the wallet balance.", title: "Error")
                }
                    
                else{
                    showAlert(msg: "You have converted \(sellTextfield.text!) \(sell!.currency) to \(receivedTextfield.text!) \(receive!.currency). Commission fee: \(charge) \(sell!.currency)", title: "Currency Converted")
                    
                    
                    
                    if let index = walletList.firstIndex(where: { $0.currency == ConverterViewController.convertedAmount?.currency }) {
                        if let i = walletList.firstIndex(where: { $0.currency == sell?.currency }) {
                            
                            walletList[i].amount = walletList[i].amount - ((sellTextfield.text?.toDouble())! + charge)
                        }
                        
                        walletList[index].amount = walletList[index].amount + ConverterViewController.convertedAmount!.amount
                        
                    }
                    else{
                        walletList.append(ConverterViewController.convertedAmount!)
                        walletCurrencies.append(ConverterViewController.convertedAmount!.currency)
                        
                        if let index = walletList.firstIndex(where: { $0.currency == sell?.currency }) {
                            
                            
                            walletList[index].amount = walletList[index].amount - ((sellTextfield.text?.toDouble())! + charge)
                        }
                        
                        
                    }
                    
                }
                currencyCollectionView.reloadData()
                sellDropDown.optionArray = walletCurrencies
            }
        }
        
        
        
        sender.resetStateWithAnimation(false)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return walletList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "currency",
                                                      for: indexPath) as! CurrencyCollectionViewCell
        cell.currencyWalletLabel.text = "\(walletList[indexPath.row].amount.rounded(toPlaces: 2)) \(walletList[indexPath.row].currency)"
        return cell
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField == sellTextfield{
            if sell != nil && receive != nil && sellTextfield.text!.count > 0{
                let sellInput = sellTextfield.text!.toDouble()
                if sell?.currency == ConverterViewController.currencyRates!.base{
                    receivedTextfield.text = "\(( sellInput! * (receive?.value)!).rounded(toPlaces: 2))"
                    ConverterViewController.convertedAmount = CurrencyWallet(amount: (self.receivedTextfield.text?.toDouble())!, currency: self.receive!.currency)
                    convertedAmountInBase = ConverterViewController.convertedAmount!.amount
                }
                else{
                    convertedAmountInBase = (sellInput!/sell!.value)
                    receivedTextfield.text = "\((convertedAmountInBase * (receive?.value)!).rounded(toPlaces: 2))"
                    ConverterViewController.convertedAmount = CurrencyWallet(amount: (self.receivedTextfield.text?.toDouble())!, currency: self.receive!.currency)
                }
            }
        }
    }
    
    
    
    
    
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
